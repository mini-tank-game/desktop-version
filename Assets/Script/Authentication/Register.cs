﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class Register : MonoBehaviour
{
    public InputField InputName;
    public InputField InputEmail;
    public InputField InputUsername;
    public InputField InputPassword;
    public Text TextStatus;

    public void ButtonRegister()
    {

        StartCoroutine(DoRegister());
    }

    IEnumerator DoRegister()
    {
        using (UnityWebRequest www = UnityWebRequest.Put(Config.API_URL_AUTH + API.REGISTER, "{\"name\": \"" + InputName.text + "\", \"email\": \"" + InputEmail.text + "\", \"username\": \"" + InputUsername.text + "\", \"password\": \"" + InputPassword.text + "\"}"))
        {
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.SendWebRequest();

            TextStatus.text = "Waiting to Register...";

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("Register Failed " + www.downloadHandler.text);

                Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.downloadHandler.text);

                TextStatus.text = obj.message;
            }
            else
            {
                Debug.Log("Register Sukses");
                TextStatus.text = "Register Sukses";

                // Parsing to object
                Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.downloadHandler.text);
            }
        }
    }
}

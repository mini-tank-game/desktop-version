﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class Login : MonoBehaviour
{
    public InputField InputUsername;
    public InputField InputPassword;
    public Text TextStatus;

    public void ButtonLogin()
    {

        StartCoroutine(DoLogin());
    }

    IEnumerator DoLogin()
    {
        using (UnityWebRequest www = UnityWebRequest.Put(Config.API_URL_AUTH + API.LOGIN, "{\"username\": \"" + InputUsername.text + "\", \"password\": \"" + InputPassword.text + "\"}"))
        {
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.SendWebRequest();

            TextStatus.text = "Waiting to Login...";

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log("Login Failed " + www.downloadHandler.text);

                Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.downloadHandler.text);

                TextStatus.text = obj.message;
            }
            else
            {
                Debug.Log("Login Sukses");
                TextStatus.text = "Login Sukses";

                // Parsing to object
                Response.AUTH_LOGIN_SUCCESS obj = JsonUtility.FromJson<Response.AUTH_LOGIN_SUCCESS>(www.downloadHandler.text);
                User.Inject(obj.data.name, obj.data.email, obj.data.username, obj.data.token);

                SceneManager.LoadScene("MainMenu");
                Debug.Log("Logged In " + User.name);
            }
        }
    }
}

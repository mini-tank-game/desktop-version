﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Peluru : MonoBehaviour
{

    private Billboard billboard;
    float counter = 0f;

    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(transform.up * 1000);
    }

    void Update()
    {
        counter += Time.deltaTime;
        if (counter >= 2f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject hit = collision.gameObject;
        HealthBar healthBar = hit.GetComponent<HealthBar>();

        if (healthBar != null)
        {
            Destroy(hit);
            if (hit.GetComponent<TankMovement>() != null)
            {
                SceneManager.LoadScene("GameOver");
            }
            // healthBar.TakeDamage(10);
        }
        Destroy(gameObject);
    }
}

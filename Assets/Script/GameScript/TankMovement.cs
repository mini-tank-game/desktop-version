﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovement : MonoBehaviour
{

    public float accelerationPower = 5f;
    public float steeringPower = 5f;
    public float steerinngAmount, speed, direction;
    public GameObject Bullet;
    public Transform bulletSpawn;
    private AudioSource suara;
    private Rigidbody2D rb;
    private Vector2 moveVelocity;

    Vector2 lastPos = new Vector2(99f, 99f);

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        suara = GetComponent<AudioSource>();
    }

    float syncCounter = 0f;

    void FixedUpdate()
    {
        //Steer tank dan pergerakannya
        steerinngAmount = -Input.GetAxis("Horizontal");
        speed = Input.GetAxis("Vertical") * accelerationPower;
        direction = Mathf.Sign(Vector2.Dot(rb.velocity, rb.GetRelativeVector(Vector2.up)));
        rb.rotation += steerinngAmount * steeringPower * rb.velocity.magnitude * direction;

        rb.AddRelativeForce(Vector2.up * speed);

        rb.AddRelativeForce(-Vector2.right * rb.velocity.magnitude * steerinngAmount / 2);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
            // suara.Play();
        }

        if (transform.position.x != lastPos.x || transform.position.y != lastPos.y)
        {
            lastPos.x = transform.position.x;
            lastPos.y = transform.position.y;
            syncCounter += Time.fixedDeltaTime;
            if (syncCounter >= 0.1f)
            {
                NetworkManager.instance.Movement(transform.position.x, transform.position.y, transform.eulerAngles.z);
                syncCounter = 0f;
            }
        }
    }

    void Fire()
    {

        GameObject b = (GameObject)(Instantiate(Bullet, bulletSpawn.position, bulletSpawn.rotation));

        // b.GetComponent<Rigidbody2D>().AddForce(transform.up * 1000);

        NetworkManager.instance.Fire(b.transform.position.x, b.transform.position.y, b.transform.eulerAngles.z);
    }

}

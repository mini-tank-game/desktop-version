﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {


    public static AudioClip fireSound, movementTank;
    static AudioSource audioSrc;

    // Use this for initialization
    void Start()
    {

        fireSound = Resources.Load<AudioClip>("shootSound");
        movementTank = Resources.Load<AudioClip>("movementTank");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "shootSound":
                audioSrc.PlayOneShot(fireSound);
                break;
            case "movementTank":
                audioSrc.PlayOneShot(movementTank);
                break;
        }
    }
}

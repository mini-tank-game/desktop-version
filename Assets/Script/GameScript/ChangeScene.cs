﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {
    
    public void HomeBtn()
    {
        SceneManager.LoadScene("Home");
    }

    public void LoginBtn()
    {
        SceneManager.LoadScene("Login");
    }

    public void PlayButton()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void RegisterBtn()
    {
        SceneManager.LoadScene("Register");
    }

    public void MainMenuBtn()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void StatistikBtn()
    {
        SceneManager.LoadScene("Statistik");
    }

    public void LeaderboardBtn()
    {
        SceneManager.LoadScene("Leaderboard");
    }

    public void Quit()
    {
        Application.Quit();
    }

}

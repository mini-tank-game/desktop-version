﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFace : MonoBehaviour {

    public GameObject bullet;

    // Update is called once per frame
    void Update ()
    {
        faceMouse();
        /*
        if (Input.GetKeyDown(KeyCode.Space))

        {

            GameObject b = (GameObject)(Instantiate(bullet, transform.position + transform.up * 1.5f, Quaternion.identity));

            b.GetComponent<Rigidbody2D>().AddForce(transform.up * 1000);

            Destroy(b, 2);

        }   
        */
    }

    void faceMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        Vector2 direction = new Vector2(
            mousePosition.x - transform.position.x,
            mousePosition.y - transform.position.y
            );

        transform.up = direction;
    }
}

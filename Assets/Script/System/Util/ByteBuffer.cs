using System.Collections.Generic;
using System;
using System.Text;

public class ByteBuffer : IDisposable
{

    private List<byte> buff;
    private byte[] readBuff;
    private int readPos;
    private bool buffUpdated = false;

    public ByteBuffer()
    {
        buff = new List<byte>();
        readPos = 0;
    }
    public long GetReadPos()
    {
        return readPos;
    }
    public byte[] ToArray()
    {
        return buff.ToArray();
    }
    public int Count()
    {
        return buff.Count;
    }
    public int Length()
    {
        return Count() - readPos;
    }
    public void Clear()
    {
        buff.Clear();
        readPos = 0;
    }

    public void WriteBytes(byte[] input)
    {
        buff.AddRange(input);
        buffUpdated = true;
    }
    public void WriteShort(short input)
    {
        buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }
    public void WriteInteger(int input)
    {
        buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }
    public void WriteFloat(float input)
    {
        buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }
    public void WriteLong(long input)
    {
        buff.AddRange(BitConverter.GetBytes(input));
        buffUpdated = true;
    }
    public void WriteString(string input)
    {
        buff.AddRange(BitConverter.GetBytes(input.Length));
        buff.AddRange(Encoding.ASCII.GetBytes(input));
        buffUpdated = true;
    }

    public int ReadInteger(bool peek = true)
    {
        if (buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = buff.ToArray();
                buffUpdated = false;
            }

            int ret = BitConverter.ToInt32(readBuff, readPos);
            if (peek & buff.Count > readPos)
                readPos += 4;

            return ret;
        }
        else
            throw new Exception("Byte Buffer is Past Limit!");
    }
    public byte[] ReadBytes(int length, bool peek = true)
    {
        if (buffUpdated)
        {
            readBuff = buff.ToArray();
            buffUpdated = false;
        }

        byte[] ret = buff.GetRange(readPos, length).ToArray();
        if (peek & buff.Count > readPos)
            readPos += length;

        return ret;
    }
    public string ReadString(bool peek = true)
    {
        int length = ReadInteger(true);
        if (buffUpdated)
        {
            readBuff = buff.ToArray();
            buffUpdated = false;
        }

        string ret = Encoding.ASCII.GetString(readBuff, readPos, length);
        if (peek & buff.Count > readPos)
            if (ret.Length > 0)
                readPos += length;

        return ret;
    }
    public short ReadShort(bool peek = true)
    {
        if (buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = buff.ToArray();
                buffUpdated = false;
            }

            short ret = BitConverter.ToInt16(readBuff, readPos);
            if (peek & buff.Count > readPos)
                readPos += 2;

            return ret;
        }
        else
            throw new Exception("Byte Buffer is Past Limit!");
    }
    public float ReadFloat(bool peek = true)
    {
        if (buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = buff.ToArray();
                buffUpdated = false;
            }

            float ret = BitConverter.ToSingle(readBuff, readPos);
            if (peek & buff.Count > readPos)
                readPos += 4;

            return ret;
        }
        else
            throw new Exception("Byte Buffer is Past Limit!");
    }
    public long ReadLong(bool peek = true)
    {
        if (buff.Count > readPos)
        {
            if (buffUpdated)
            {
                readBuff = buff.ToArray();
                buffUpdated = false;
            }

            long ret = BitConverter.ToInt64(readBuff, readPos);
            if (peek & buff.Count > readPos)
                readPos += 8;

            return ret;
        }
        else
            throw new Exception("Byte Buffer is Past Limit!");
    }

    private bool disposedValue = false;
    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
                buff.Clear();
        }

        disposedValue = true;
    }
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

}

public class User
{
    public static string name;
    public static string email;
    public static string username;
    public static string token;

    public static void Inject(string name, string email, string username, string token)
    {
        User.name = name;
        User.email = email;
        User.username = username;
        User.token = token;
    }

    public static void Clear()
    {
        User.name = "";
        User.email = "";
        User.username = "";
        User.token = "";
    }
}

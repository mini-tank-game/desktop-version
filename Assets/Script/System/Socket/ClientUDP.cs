using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

//Type of packet transfer from client
public enum Packets
{
    S_JOIN = 1,
    S_TRANSFORM,
    S_DISCONNECT,
    S_FIRE
};

public class ClientUDP : MonoBehaviour
{

    public static ClientUDP instance;
    public string ipServer = "128.199.221.36";
    public int portServer = 1000;

    public Byte[] receivedByte;
    private HandlePackets handlePackets;

    bool handleData = false;

    UdpClient socket;

    private void Awake()
    {
        InitializeUDP();
        handlePackets = new HandlePackets();
        instance = this;

        //JoinGame();
    }

    private void Update()
    {
        if (handleData == true)
        {
            handlePackets.HandleData(receivedByte);

            handleData = false;

            //Receive again
            socket.BeginReceive(OnReceive, null);
        }
    }

    public void InitializeUDP()
    {
        socket = new UdpClient(ipServer, portServer);

        socket.BeginReceive(OnReceive, null);
    }

    private void OnReceive(IAsyncResult result)
    {

        IPEndPoint endPoint = null;
        receivedByte = socket.EndReceive(result, ref endPoint);

        //To handle type of packet
        handleData = true;
    }

    public void SendJoin(string username)
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteLong((long)Packets.S_JOIN);
        buffer.WriteString(username);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }

    public void SendPositionAndRotation(float xPos, float yPos, float zRot)
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteLong((long)Packets.S_TRANSFORM);
        buffer.WriteFloat(xPos);
        buffer.WriteFloat(yPos);
        buffer.WriteFloat(zRot);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }

    public void SendBullet(float xPos, float yPos, float zRot)
    {
        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteLong((long)Packets.S_FIRE);
        buffer.WriteFloat(xPos);
        buffer.WriteFloat(yPos);
        buffer.WriteFloat(zRot);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }

    public void JoinGame()
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteLong((long)Packets.S_JOIN);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);
    }

    public void Close()
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteLong((long)Packets.S_DISCONNECT);

        socket.Send(buffer.ToArray(), buffer.ToArray().Length);

        socket.Close();
        socket = null;
    }


}

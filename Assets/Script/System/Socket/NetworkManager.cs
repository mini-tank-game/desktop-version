using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;
    public CameraController cameraa;
    public GameObject playerTank;
    public GameObject enemyTank;
    public GameObject bullet;

    ClientUDP client;

    private void Awake()
    {
        // DontDestroyOnLoad(this);
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        client = GetComponent<ClientUDP>();
        AttempConnect();
    }

    public void AttempConnect()
    {
        // SceneManager.LoadScene("Gameplay");
        client.SendJoin(User.username);
    }

    public void Movement(float xPos, float yPos, float zRot)
    {
        client.SendPositionAndRotation(xPos, yPos, zRot);
    }

    public void Fire(float xPos, float yPos, float zRot)
    {
        client.SendBullet(xPos, yPos, zRot);
    }

    private void OnApplicationQuit()
    {
        client.Close();
    }
}

using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandlePackets : MonoBehaviour
{
    public static ByteBuffer playerBuffer;
    private static long pLength;
    private delegate void Packet_(byte[] data);
    private static Dictionary<long, Packet_> packets;
    public Dictionary<string, GameObject> enemiesTank = new Dictionary<string, GameObject>();

    public HandlePackets()
    {
        InitializePackets();
    }

    private void InitializePackets()
    {
        packets = new Dictionary<long, Packet_>();
        packets.Add((long)Packets.S_JOIN, PACKET_JOIN);
        packets.Add((long)Packets.S_TRANSFORM, PACKET_TRANSFORM);
        packets.Add((long)Packets.S_DISCONNECT, PACKET_DC);
        packets.Add((long)Packets.S_FIRE, PACKET_FIRE);
    }

    public void HandleData(byte[] data)
    {
        byte[] Buffer;
        Buffer = (byte[])data.Clone();

        if (playerBuffer == null)
        {
            playerBuffer = new ByteBuffer();
        }

        playerBuffer.WriteBytes(Buffer);

        if (playerBuffer.Count() == 0)
        {
            playerBuffer.Clear();
            return;
        }

        if (playerBuffer.Length() >= 8)
        {
            pLength = playerBuffer.ReadLong(false);

            if (pLength <= 0)
            {
                playerBuffer.Clear();
                return;
            }
        }

        playerBuffer.ReadLong();

        HandleDataPackets(data);

        playerBuffer.Clear();
        return;
    }

    public void HandleDataPackets(byte[] data)
    {
        long packetIdentifier;
        ByteBuffer buffer;
        Packet_ packet;

        buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        packetIdentifier = buffer.ReadLong();

        buffer.Dispose();

        if (packets.TryGetValue(packetIdentifier, out packet))
        {
            packet.Invoke(data);
        }

    }

    private void PACKET_JOIN(byte[] data)
    {
        // Server.counterPlayer++;
        Debug.Log("NEW Packet JOIN");

        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadLong();

        buffer.ReadInteger(); //id
        string username = buffer.ReadString(); //username
        float zRot = buffer.ReadFloat(); //zRot
        float xPos = buffer.ReadFloat(); //xPos
        float yPos = buffer.ReadFloat(); //yPos

        // Instance enemy or player
        GameObject t = Instantiate(username != User.username ? NetworkManager.instance.enemyTank : NetworkManager.instance.playerTank, new Vector2(xPos, yPos), Quaternion.Euler(0, 0, zRot));
        if (username == User.username)
            NetworkManager.instance.cameraa.targets[0] = t.transform;
        else
            enemiesTank.Add(username, t);
    }

    private void PACKET_TRANSFORM(byte[] data)
    {
        Debug.Log("Dapat packet transform");

        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadLong();

        float xPos = buffer.ReadFloat(); //xPos
        float yPos = buffer.ReadFloat(); //yPos
        float zRot = buffer.ReadFloat(); //zRot
        string username = buffer.ReadString();

        enemiesTank[username].transform.eulerAngles = new Vector3(0, 0, zRot);
        enemiesTank[username].transform.position = new Vector2(xPos, yPos);
    }

    private void PACKET_FIRE(byte[] data)
    {
        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadLong();

        float xPos = buffer.ReadFloat(); //xPos
        float yPos = buffer.ReadFloat(); //yPos
        float zRot = buffer.ReadFloat(); //zRot
        string username = buffer.ReadString();

        GameObject g = Instantiate(NetworkManager.instance.bullet, new Vector2(xPos, yPos), Quaternion.Euler(0, 0, zRot));
    }

    private void PACKET_DC(byte[] data)
    {
        Console.WriteLine("Packet DC");

        ByteBuffer buffer = new ByteBuffer();
        buffer.WriteBytes(data);
        buffer.ReadLong();

        string username = buffer.ReadString();

        Destroy(enemiesTank[username]);
        enemiesTank.Remove(username);
    }
}

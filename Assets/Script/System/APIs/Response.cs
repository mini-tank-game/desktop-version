using System;
using System.Collections.Generic;

public class Response
{
    [Serializable]
    public class BASE_RESPONSE
    {
        public string status;
        public string message;
    }

    // Login Response
    [Serializable]
    public class AUTH_LOGIN_SUCCESS_DATA
    {
        public string name;
        public string username;
        public string email;
        public string token;
    }

    [Serializable]
    public class AUTH_LOGIN_SUCCESS : BASE_RESPONSE
    {
        public AUTH_LOGIN_SUCCESS_DATA data;
    }
}
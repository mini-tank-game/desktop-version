using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class API
{
    /* Authentication API */
    public static readonly string LOGIN = "auth/login";
    public static readonly string REGISTER = "auth/register";

    public static IEnumerator Request(string url, Func<WWWForm, Dictionary<string, string>> setup, Func<WWW, int> callbackSuccess, Func<WWW, int> callbackError)
    {
        WWWForm form = new WWWForm();

        // Get headers
        Dictionary<string, string> headers = setup(form);

        WWW www = new WWW(url, form.data.Length > 0 ? form.data : null, headers);
        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            callbackError(www);
        }
        else
        {
            callbackSuccess(www);
        }
    }
}
